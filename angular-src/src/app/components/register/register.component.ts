import {Component, OnInit} from '@angular/core';
import {ValidationService} from '../../services/validation.service';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {ToastService} from "../../services/toast.service";

declare var M: any;

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    name: String;
    username: String;
    email: String;
    password: String;
    file: String;

    constructor(private validationService: ValidationService, private authService: AuthService,
                private router: Router, private toastService: ToastService) {}

    ngOnInit() {
        const user = {
            name: this.name,
            email: this.email,
            username: this.username,
            password: this.password
        };
        if (!this.validationService.validateRegister(user)) {
            this.toastService.error('All fields must be filed');
            return false;
        }

        this.authService.registerUser(user).subscribe(data => {
            if (data.success) {
                this.toastService.success(data.msg);
                this.router.navigate(['/login']);
            } else {
                this.toastService.error(data.msg);
            }
        });
    }

    onRegisterSubmit() {
        const user = {
            name: this.name,
            email: this.email,
            username: this.username,
            password: this.password
        };

        if (!this.validationService.validateRegister(user)) {
            this.toastService.error('All fields must be filed');
            return false;
        }

        this.authService.registerUser(user).subscribe(data => {
            if (data.success) {
                this.toastService.success(data.msg);
                this.router.navigate(['/login']);
            } else {
                this.toastService.error(data.msg);
            }
        });
    }

}

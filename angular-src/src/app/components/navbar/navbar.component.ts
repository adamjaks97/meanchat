import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import { ConfirmationModalService } from "../../services/confirmation-modal.service";
import { ToastService } from "../../services/toast.service";

declare var M: any;

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
    getAvatar: any;
    avatar: any;
    constructor( private authService: AuthService, private router: Router,
                 private confirmationModalService: ConfirmationModalService,
                 private toastService: ToastService) {}

    ngOnInit() {
        this.confirmationModalService.modal = document.querySelector('#modal');
        this.confirmationModalService.modalInstance = M.Modal.init(this.confirmationModalService.modal);
        if (this.authService.loggedIn()) {
            this.getImg(this.authService.getUserData().id);
        }
    }

    ngOnChanges() {
        if (this.authService.loggedIn()) {
            this.getImg(this.authService.getUserData().id);
        }
    }

    openLogoutModal() {
        this.confirmationModalService.content = 'Are you sure you want to logout?';
        this.confirmationModalService.cta = 'Logout';
        this.confirmationModalService.proceed = () => {
            this.authService.logout();
            this.toastService.success('Logged out');
            this.confirmationModalService.modalInstance.close();
            this.router.navigate(['/']);
        };
        this.confirmationModalService.modalInstance.open();
    }

    getImg(id) {
        this.authService.getAvatar(id).subscribe(data => {
            this.avatar = data.avatar;
        });
    }
}

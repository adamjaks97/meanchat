import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {ToastService} from "../../services/toast.service";

declare var M: any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    username: String;
    password: String;

    constructor( private authService: AuthService, private router: Router,
                 private toastService: ToastService) {}

    ngOnInit() {}

    onLoginSubmit() {
        const user = {
          username: this.username,
          password: this.password
        };

        this.authService.authenticateUser(user).subscribe(data => {
            if (data.success) {
                this.authService.storeUserData(data.token, data.user);
                this.toastService.success(data.msg);
                this.router.navigate(['']);
            } else {
                this.toastService.error(data.msg);
            }
        });
    }
}

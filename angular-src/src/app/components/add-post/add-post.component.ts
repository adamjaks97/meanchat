import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {PostsService} from "../../services/posts.service";
import {Router} from "@angular/router";
import {LoaderService} from "../../services/loader.service";
import {ToastService} from "../../services/toast.service";
import {FileUploader} from "ng2-file-upload";

@Component({
    selector: 'app-add-post',
    templateUrl: './add-post.component.html',
    styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit {
    content: string;
    posts: any;
    uploader: any;

    constructor(private authService: AuthService, private router: Router,
                private postsService: PostsService, private loaderService: LoaderService,
                private toastService: ToastService) {
    }

    ngOnInit() {

        this.uploader = new FileUploader({
            url: 'http://localhost:3000/posts/add/',
            itemAlias: 'attached-img',
        });

        this.uploader.onBeforeUploadItem = (item: FileUploader | any) => {
            item.withCredentials = false;
            let user = JSON.parse(localStorage.getItem('user'));

            item.formData.push({
                "creator": {
                    id: user.id,
                    name: user.name,
                    avatar: user.avatar
                },
                "content": "value"
            });

        };

        /*this.uploader.onBeforeUploadItem = (item) => {
            item.withCredentials = false;

            let user = JSON.parse(localStorage.getItem('user'));

            item.formData['creator'] = {
                id: user.id,
                name: user.name,
                avatar: user.avatar
            };
            item.formData['content'] = 'value';


            //
            // let creator = {
            //     id: user.id,
            //     name: user.name,
            //     avatar: user.avatar
            // };

            // this.uploader.uploadAll();
            // const post = {
            //     content: document.querySelector('.textfield').innerHTML,
            //     creator: creator,
            //     post_date: Date.now()
            // };

            // this.postsService.addPost(post).subscribe(data => {
            //     if (data.success) {
            //         this.toastService.success(data.msg);
            //         this.loaderService.isLoading = true;
            //         this.postsService.getPosts().subscribe(response => {
            //             this.postsService.getPostHandling(response);
            //             this.loaderService.hideLoader();
            //         }, err => {
            //             this.toastService.error(err);
            //             return false;
            //         });
            //     } else {
            //         this.toastService.error(data.msg);
            //     }
            // });

        };*/

        this.uploader.onAfterAddingFile = (file) => {
            console.log(file);
        };


        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {

            // this.postsService.getPosts().subscribe(response => {
            //     this.postsService.getPostHandling(response);
            //     this.loaderService.hideLoader();
            // }, err => {
            //     this.toastService.error(err);
            //     return false;
            // });
            // this.authService.increasePostAmount(JSON.parse(localStorage.getItem('user')).id)
            //     .subscribe(() => {
            //     });
        };

    }

    onAddPostSubmit() {
        if (this.authService.loggedIn()) {
            let user = JSON.parse(localStorage.getItem('user'));

            let fd = new FormData();
            fd.append('content', document.querySelector('.textfield').innerHTML);
            fd.append('creator_id', user.id);
            fd.append('creator_name', user.name);
            fd.append('post_date', Date.now());
            fd.append('attached-img',
                (document.querySelector('#add-image-input') as HTMLInputElement).files[0]);

            this.postsService.addPost(fd).subscribe(data => {
                if (data.success) {
                    this.toastService.success(data.msg);
                    this.loaderService.isLoading = true;
                    this.postsService.getPosts().subscribe(response => {
                        this.postsService.getPostHandling(response);
                        this.loaderService.hideLoader();
                    }, err => {
                        this.toastService.error(err);
                        return false;
                    });
                } else {
                    this.toastService.error(data.msg);
                }
            });

            this.authService.increasePostAmount(JSON.parse(localStorage.getItem('user')).id)
                .subscribe(() => {
                });
        }
    }
}

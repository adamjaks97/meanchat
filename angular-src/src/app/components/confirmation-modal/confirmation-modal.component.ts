import {Component, OnInit} from '@angular/core';
import { ConfirmationModalService } from "../../services/confirmation-modal.service";

@Component({
    selector: 'app-confirmation-modal',
    templateUrl: './confirmation-modal.component.html',
    styleUrls: ['./confirmation-modal.component.scss']
})
export class ConfirmationModalComponent implements OnInit {

    constructor(private confirmationModalService: ConfirmationModalService) {}
    ngOnInit() {}

}

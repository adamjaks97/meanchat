import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {PostsService} from "../../services/posts.service";
import {LoaderService} from "../../services/loader.service";
import {FileUploader} from 'ng2-file-upload/ng2-file-upload';
import {ToastService} from "../../services/toast.service";

declare var M: any;

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss']
})

export class UserProfileComponent implements OnInit {
    user: Object;
    posts: Object;
    loggedUser: String;
    avatarModal: any;
    avatarModalInstance: any;
    uploader: any;
    hasBaseDropZoneOver: false;

    constructor(private authService: AuthService, private router: Router, private postsService: PostsService,
                private loaderService: LoaderService, private toastService: ToastService) {
    }

    ngOnInit() {
        this.loggedUser = JSON.parse(localStorage.getItem('user')).id;

        this.uploader = new FileUploader({
            url: `http://localhost:3000/users/avatar/${this.loggedUser}`,
            itemAlias: 'avatar'
        });

        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {

        };

        this.loaderService.isLoading = true;
        this.postsService.getPosts().subscribe(response => {
            this.posts = response.posts.reverse();
        }, err => {
            throw err;
        });

        this.authService.getProfile().subscribe(profile => {
            this.user = profile.user;
            this.loaderService.hideLoader();
        }, err => {
            throw err;
        });
    }

    ngAfterViewInit() {
        this.setModalInstance((modal) => {
            this.avatarModalInstance = M.Modal.init(modal);
        });
    }

    setModalInstance(callback) {
        this.avatarModal = document.querySelector('#avatar-modal');
        callback(this.avatarModal);
    }

    onAvatarChange(event) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]);
            reader.onload = (event) => {
                this.user['avatar'] = event.target['result'];
            };
        }
    }

    onCloseAvatarModal() {
        this.toastService.success('Avatar changed');
    }

    fileOverBase(e) {
        this.hasBaseDropZoneOver = e;
    }

    showDropFilePreview(event) {
        let reader = new FileReader();
        reader.readAsDataURL(event[0]);
        reader.onload = (event) => {
            this.user['avatar'] = event.target['result'];
        };
    }

}

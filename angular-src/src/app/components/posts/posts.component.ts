import {Component, OnInit, Input} from '@angular/core';
import {Router} from "@angular/router";
import {PostsService} from "../../services/posts.service";
import {AuthService} from "../../services/auth.service";
import {ConfirmationModalService} from "../../services/confirmation-modal.service";
import {LoaderService} from "../../services/loader.service";
import {ToastService} from "../../services/toast.service";
import {FileUploader} from "ng2-file-upload";

declare var M: any;

@Component({
    selector: 'app-posts',
    templateUrl: './posts.component.html',
    styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
    avatar: string;
    loggedUser: any;
    modalId: any;
    editModal: any;
    editModalInstance: any;
    content: string;
    contentEdited: string;
    orderSelect: any;
    orderSelectInstance: any;
    showButton: boolean;
    getAvatar: any;
    uploader: any;

    constructor(private postsService: PostsService, private authService: AuthService,
                private router: Router, private confirmationModalService: ConfirmationModalService,
                private loaderService: LoaderService, private toastService: ToastService) {
    }

    ngOnInit() {

        this.showButton = true;

        this.editModal = document.querySelector('#edit-modal');
        this.editModalInstance = M.Modal.init(this.editModal);

        this.orderSelect = document.querySelectorAll('#post-order-select');
        this.orderSelectInstance = M.FormSelect.init(this.orderSelect);

        this.orderSelect = 'post_date';

        this.loggedUser = localStorage.getItem('user') ?
            JSON.parse(localStorage.getItem('user')).id : '';

        this.loaderService.isLoading = true;

        this.postsService.getPosts().subscribe(response => {
            this.postsService.getPostHandling(response);
            this.loaderService.hideLoader();
        }, err => {
            this.toastService.error(err);
            this.loaderService.hideLoader();
            return false;
        });

        this.uploader = new FileUploader({
            url: `http://localhost:3000/users/avatar/${this.loggedUser}`,
            itemAlias: 'avatar'
        });

        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {

        };
    }

    loadMore() {
        this.loaderService.isLoadingSmall = true;
        if (this.orderSelect === 'post_date') {
            this.postsService.getPosts(true).subscribe(response => {
                if (this.postsService.postIndex > response.posts.length) {
                    this.showButton = false;
                }
                this.postsService.getPostHandling(response);
                this.loaderService.hideLoaderSmall();
            }, err => {
                this.toastService.error(err);
                this.loaderService.hideLoaderSmall();
                return false;
            });
        } else if (this.orderSelect === 'rate') {
            this.postsService.getPostsOrdered(true).subscribe(response => {
                if (this.postsService.postIndex > response.posts.length) {
                    this.showButton = false;
                }
                this.postsService.getPostHandling(response);
                this.loaderService.hideLoaderSmall();
            }, err => {
                this.toastService.error(err);
                this.loaderService.hideLoaderSmall();
                return false;
            });
        }
    }

    order(orderSelect) {
        this.loaderService.isLoading = true;
        if (orderSelect === 'post_date') {
            this.postsService.getPosts().subscribe(response => {
                this.postsService.getPostHandling(response);
            }, err => {
                this.toastService.error(err);
                return false;
            });
        } else {
            this.loaderService.isLoading = true;
            this.postsService.getPostsOrdered().subscribe(response => {
                this.postsService.getPostHandling(response);
            }, err => {
                this.toastService.error(err);
                return false;
            });
        }
        this.loaderService.hideLoader();
    }

    onEditModalSubmit() {
        this.editModalInstance.close();
        this.postsService.editPost(this.modalId, this.contentEdited).subscribe(data => {

            if (data.success) {
                this.loaderService.isLoading = true;
                this.postsService.getPosts().subscribe(response => {
                    this.postsService.getPostHandling(response);
                }, err => {
                    this.toastService.error(err);
                    return false;
                });
                this.loaderService.hideLoader();
                this.toastService.success(data.msg);
            } else {
                console.log('some error');
            }
        });
    }

    openEditModal(id, content) {
        this.modalId = id;
        this.editModal.querySelector('#textarea-edit').value = content;
        this.editModalInstance.open();
    }

    deletePost(id) {
        this.confirmationModalService.content = 'Are you sure you want to delete this post?';
        this.confirmationModalService.cta = 'Delete';
        this.confirmationModalService.proceed = () => {
            this.postsService.deletePost(id).subscribe(data => {
                if (data.success) {
                    this.toastService.success(data.msg);
                    this.loaderService.isLoading = true;
                    this.postsService.getPosts().subscribe(response => {
                        this.postsService.getPostHandling(response);
                        this.loaderService.hideLoader();
                    }, err => {
                        this.toastService.error(err);
                        return false;
                    });
                } else {
                    this.toastService.error(data.msg);
                }

            });
            this.authService.decreasePostAmount(JSON.parse(localStorage.getItem('user')).id)
                .subscribe(() => {
                });
            this.confirmationModalService.modalInstance.close();
        };
        this.confirmationModalService.modalInstance.open();
    }

    ratePost(id, creator_id) {
        this.postsService.ratePost(id, creator_id).subscribe(() => {
            if (this.orderSelect === 'post_date') {
                this.postsService.getPosts().subscribe(response => {
                    this.postsService.getPostHandling(response);
                }, err => {
                    this.toastService.error(err);
                    return false;
                });
            } else if (this.orderSelect === 'rate') {
                this.loaderService.isLoading = true;
                this.postsService.getPostsOrdered().subscribe(response => {
                    this.postsService.getPostHandling(response);
                    this.loaderService.hideLoader();
                }, err => {
                    this.toastService.error(err);
                    this.loaderService.hideLoader();
                    return false;
                });
            }
        });
    }

    revertRate(id, loggedUser_id) {
        this.postsService.revertRatePost(id, loggedUser_id).subscribe(() => {
            if (this.orderSelect === 'post_date') {
                this.postsService.getPosts().subscribe(response => {
                    this.postsService.getPostHandling(response);
                }, err => {
                    this.toastService.error(err);
                    return false;
                });
            } else if (this.orderSelect === 'rate') {
                this.loaderService.isLoading = true;
                this.postsService.getPostsOrdered().subscribe(response => {
                    this.postsService.getPostHandling(response);
                    this.loaderService.hideLoader();
                }, err => {
                    this.toastService.error(err);
                    this.loaderService.hideLoader();
                    return false;
                });
            }
        });
    }

    isLiked(users_liked) {
        return users_liked.indexOf(this.loggedUser) > -1;
    }

    getCreatorId(post) {
        return post.creator[0]['id'];
    }

    getCreatorName(post) {
        return post.creator[0]['name'];
    }
}

import {Injectable} from '@angular/core';

@Injectable()
export class ConfirmationModalService {
    modal: any;
    modalInstance: any;
    content: String;
    cta: String;
    proceed;

    constructor() {}

    closeModal() {
        this.modalInstance.close();
    }

}

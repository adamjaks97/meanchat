import {Injectable} from '@angular/core';

@Injectable()
export class LoaderService {
    isLoading: boolean;
    isLoadingSmall: boolean;

    constructor() { this.isLoading = false; this.isLoadingSmall = false; }

    hideLoader() {
        setTimeout(() => {
            this.isLoading = false;
        }, 500);
    }

    hideLoaderSmall() {
        setTimeout(() => {
            this.isLoadingSmall = false;
        }, 500);
    }
}

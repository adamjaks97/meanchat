import {Injectable} from '@angular/core';

@Injectable()
export class ValidationService {

    constructor() {}

    validateRegister(user) {
        return !(user.name == undefined || user.email == undefined
            || user.username == undefined || user.password == undefined)
    }
}

import {Injectable} from '@angular/core';

declare var M: any;

@Injectable()
export class ToastService {
    constructor() {}

    success = msg => {
        return M.toast({html: `<i class="material-icons">check</i> ${msg}`, classes: 'white deep-orange-text' });
    };

    error = msg => {
        return M.toast({html: `<i class="material-icons">close</i> ${msg}`, classes: 'grey'});
    }
}

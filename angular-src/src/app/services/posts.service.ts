import {Injectable} from '@angular/core';
import {Http, Headers} from "@angular/http";

@Injectable()
export class PostsService {
    posts: any;
    postAmount = 10;
    postIndex = this.postAmount;

    constructor(private http: Http) {}

    getPostHandling(response) {
        this.posts = response.posts.reverse().slice(0, this.postIndex);
    }

    getPosts(loadMore?) {
        if (loadMore) {
            this.postIndex += this.postAmount;
        } else {
            this.postIndex = this.postAmount;
        }
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get('http://localhost:3000/posts/fetch', {headers: headers})
            .map(res => res.json());
    }

    getPostsOrdered(loadMore?) {
        if (loadMore) {
            this.postIndex += this.postAmount;
        } else {
            this.postIndex = this.postAmount;
        }
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get('http://localhost:3000/posts/order', {headers: headers})
            .map(res => res.json());
    }

    addPost(post) {
        return this.http.post('http://localhost:3000/posts/add', post)
            .map(res => res.json());
    }

    deletePost(id) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(`http://localhost:3000/posts/delete/${id}`, {headers: headers})
            .map(res => res.json());
    }

    editPost(id, content) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(`http://localhost:3000/posts/edit/${id}`,
            {headers: headers, content: content})
            .map(res => res.json());
    }

    ratePost(id, creator_id) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(`http://localhost:3000/posts/rate/${id}`,
            {headers: headers, creator_id: creator_id})
            .map(res => res.json());
    }

    revertRatePost(id, loggedUser_id) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(`http://localhost:3000/posts/revert/${id}`,
            {headers: headers, loggedUser_id: loggedUser_id})
            .map(res => res.json());
    }
}
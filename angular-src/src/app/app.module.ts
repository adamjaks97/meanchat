//modules

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule, Routes} from '@angular/router';

//components

import {AppComponent} from './app.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {FeedComponent} from './components/feed/feed.component';
import {UserProfileComponent} from './components/user-profile/user-profile.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { ConfirmationModalComponent } from "./components/confirmation-modal/confirmation-modal.component";
import { PostsComponent } from './components/posts/posts.component';
import { AddPostComponent } from './components/add-post/add-post.component';

//services

import { ValidationService } from './services/validation.service';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { PostsService } from './services/posts.service';
import { ConfirmationModalService } from "./services/confirmation-modal.service";
import { LoaderService } from "./services/loader.service";
import { ToastService } from "./services/toast.service";
import { TimeagoPipe } from './pipes/timeago.pipe';
import { TextEditorDirective } from './directives/text-editor.directive';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';

const appRoutes: Routes = [
    {path: '', component: FeedComponent},
    {path: 'profile', component: UserProfileComponent, canActivate: [AuthGuardService]},
    {path: 'register', component: RegisterComponent},
    {path: 'login', component: LoginComponent}
];

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        NavbarComponent,
        FeedComponent,
        UserProfileComponent,
        RegisterComponent,
        LoginComponent,
        ConfirmationModalComponent,
        PostsComponent,
        AddPostComponent,
        TimeagoPipe,
        TextEditorDirective,
        TimeagoPipe,
        FileSelectDirective,
        FileDropDirective
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(appRoutes),
],
providers: [ ValidationService, AuthService, AuthGuardService, PostsService, ConfirmationModalService,
    LoaderService, ToastService],
    bootstrap
:
[AppComponent]
})

export class AppModule {
}

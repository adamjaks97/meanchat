import {Directive, ViewContainerRef, TemplateRef} from '@angular/core';
import {FileUploader} from "ng2-file-upload";

@Directive({
    selector: '[text-editor]'
})
export class TextEditorDirective {
    context: any;

    constructor(private viewContainerRef: ViewContainerRef,
                private templateRef: TemplateRef<any>) {
    }

    ngOnInit() {
        this.context = {
            scope: {
                format: (style, param?, content?) => {
                    document.execCommand(style, param, content);
                },
                addImage: (event) => {
                    if (event.target.files && event.target.files[0]) {
                        let reader = new FileReader();
                        reader.readAsDataURL(event.target.files[0]);
                        reader.onload = (event) => {
                            this.context.scope.imageLoaded = true;
                            this.context.scope.addImgPath = event.target['result'];
                        }
                    }
                }
            }
        };

        const embeddedView = this.viewContainerRef.createEmbeddedView(this.templateRef, this.context);

        let textField = document.querySelector('.textfield');
        let emoji = {
            ':)': '<img src="../../assets/img/emoji/happy.png">',
            ':(': '<img src="../../assets/img/emoji/sad.png">',
            ':|': '<img src="../../assets/img/emoji/neutral.png">'
        };
        textField.addEventListener('input', (e) => {
            textField.innerHTML = textField.innerHTML.replace(/\:\)|\:\(|\:\|/gi, (matched) => {
                return emoji[matched];
            });
            let nodes = textField.childNodes;
            if (nodes.length) {
                for (let i = 0; i < nodes.length; i++) {
                    let matches = nodes[i].textContent.match(/\:\)|\:\(|\:\|/gi);
                    if (matches) {
                        let e = matches[0];
                        let start = nodes[i].textContent.indexOf(e);
                        let end = start + e.length;
                        textField.innerHTML = textField.innerHTML.replace(/\:\)|\:\(|\:\|/gi, (matched) => {
                            return emoji[matched];
                        });
                        let range = document.createRange();
                        range.selectNodeContents(textField);
                        range.setStartAfter(nodes[i]);

                        let sel = window.getSelection();
                        sel.removeAllRanges();
                        sel.addRange(range);
                    }
                }
            }
            let range = document.createRange();
            range.selectNodeContents(textField);
            range.setStartAfter(nodes[nodes.length-1]);

            let sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        }, false);
    }

}

document.addEventListener("DOMContentLoaded", function(e) {

    M.AutoInit();

    document.querySelector('.back').addEventListener('click', () => {
        window.history.back();
    }, false);

    const toastSuccess = {
        html: `<i class="material-icons">check</i> ${data.msg}`,
        classes: 'white deep-orange-text'
    };

});
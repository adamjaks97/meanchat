const mongoose = require('mongoose');
const config = require('./config/database');
const User = require('./models/user');
const Post = require('./models/post');
const bcrypt = require('bcryptjs');

const content = 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis ' +
    'praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi ' +
    'sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, ' +
    'id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero' +
    ' tempore, cum soluta nobis est eligendi optio cumque nihil ' +
    'impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda ' +
    'est, omnis dolor repellendus.';

hashPassword = pass => {
    return bcrypt.hashSync(pass, bcrypt.genSaltSync(10));
};

let users = [
    {
        name: 'John Doe',
        email: 'johndoe@meanchat.com',
        username: 'johndoe',
        password: hashPassword('pass'),
        post_amount: 2
    },
    {
        name: 'Jenny Doe',
        email: 'jenny@meanchat.com',
        username: 'jennydoe',
        password: hashPassword('pass'),
        post_amount: 2
    },
    {
        name: 'Laura Kovalsky',
        email: 'kovalskylaura@meanchat.com',
        username: 'laurakovalsky',
        password: hashPassword('pass'),
        post_amount: 4
    },
    {
        name: 'Adam Jones',
        email: 'adam.jones@meanchat.com',
        username: 'adamjones',
        password: hashPassword('pass'),
        post_amount: 4
    },
    {
        user_id: 5,
        name: 'Ian Corby',
        email: 'iancorby@meanchat.com',
        username: 'iancorby',
        password: hashPassword('pass'),
        post_amount: 4
    }
];

addUsers = (callback) => {
    let usersIds = [];
    User.insertMany(users).then(results => {
       results.map((user) => {
            usersIds.push({
                name: user.name,
                id: user._id
            })
        });
        callback(usersIds);
    });

};

fetchId = (usersIds, name) => {
    let id = null;
    usersIds.map((user) => {
        if (user.name === name) {
            id = user.id;
        }
    });
    return id;
};

mongoose.connect(config.database, (err, db) => {
    db.collection('posts').drop();
    db.collection('users').drop();
    addUsers((usersIds) => {
        let posts = [
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'Ian Corby'),
                creator_name: 'Ian Corby',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'Laura Kovalsky'),
                creator_name: 'Laura Kovalsky',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'Laura Kovalsky'),
                creator_name: 'Laura Kovalsky',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'Ian Corby'),
                creator_name: 'Ian Corby',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'Adam Jones'),
                creator_name: 'Adam Jones',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'Jenny Doe'),
                creator_name: 'Jenny Doe',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'Adam Jones'),
                creator_name: 'Adam Jones',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'John Doe'),
                creator_name: 'John Doe',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'Ian Corby'),
                creator_name: 'Ian Corby',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'Laura Kovalsky'),
                creator_name: 'Laura Kovalsky',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'Laura Kovalsky'),
                creator_name: 'Laura Kovalsky',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'Ian Corby'),
                creator_name: 'Ian Corby',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'Adam Jones'),
                creator_name: 'Adam Jones',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'Jenny Doe'),
                creator_name: 'Jenny Doe',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'Adam Jones'),
                creator_name: 'Adam Jones',
                post_date: new Date(),
                rate: 0
            },
            {
                users_liked: [],
                content: content,
                creator_id: fetchId(usersIds, 'John Doe'),
                creator_name: 'John Doe',
                post_date: new Date(),
                rate: 0
            }
        ];
        db.collection('posts').insertMany(posts, () => {
            console.log('Success');
            mongoose.connection.close();
        });
    });
});
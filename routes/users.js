const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const config = require('../config/database');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, './uploads');
    },
    filename: (req, file, callback) => {
        callback(null, file.originalname);
    }
});

const fileFilter = (req, file, callback) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        callback(null, true);
    } else {
        callback(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
}).single('avatar');

router.post('/avatar/:id', (req, res) => {
    User.findOne({_id: req.params.id}, (err, user) => {
        if (err) {
            return res.send(err);
        }
        upload(req, res, (err) => {
            if (err) {
                return res.send(err);
            }
            user.avatar = 'http://localhost:3000/uploads/' + req.file.filename;
            user.save();
        })
    });
});

router.post('/register', (req, res) => {
    let newUser = new User({
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        post_amount: 0,
        avatar: ''
    });

    User.getUserByUsername(newUser.username, (err, user) => {
        if (err) throw err;
        if (!user) {
            User.addUser(newUser, (err) => {
                if (err) {
                    console.log(newUser);
                    res.json({success: false, msg: 'Failed to register user'});
                } else {
                    console.log(newUser);
                    res.json({success: true, msg: 'User registered'});
                }
            });
        } else {
            return res.json({success: false, msg: 'User with this username is already registered'});
        }
    });
});

router.post('/authenticate', (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    User.getUserByUsername(username, (err, user) => {
        if (err) throw err;
        if (!user) {
            return res.json({success: false, msg: 'User not found'});
        }
        User.comparePassword(password, user.password, (err, isMatch) => {
            if (err) throw err;
            if (isMatch) {
                const token = jwt.sign(user.toJSON(), config.secret, {
                    expiresIn: 604800
                });

                res.json({
                    success: true,
                    msg: `Logged as ${user.name}`,
                    token: `JWT ${token}`,
                    user: {
                        id: user._id,
                        name: user.name,
                        username: user.username,
                        email: user.email,
                        avatar: user.avatar
                    }
                })
            } else {
                return res.json({success: false, msg: 'Password is incorrect'});
            }
        });
    });
});

router.get('/profile', passport.authenticate('jwt', {session: false}), (req, res) => {
    res.json({user: req.user});
});

router.post('/increase/:id', (req, res) => {
    User.updateOne({_id: req.params.id}, {$inc: {post_amount: 1}}, (err) => {
        if (err) {
            res.json({success: false, msg: err._message});
        } else {
            res.json({success: true, msg: 'Post amount increased'});
        }
    });
});

router.post('/decrease/:id', (req, res) => {
    User.updateOne({_id: req.params.id}, {$inc: {post_amount: -1}}, (err) => {
        if (err) {
            res.json({success: false, msg: err._message});
        } else {
            res.json({success: true, msg: 'Post amount decreased'});
        }
    });
});

router.get('/get-avatar/:id', (req, res) => {
    User.findOne({_id: req.params.id}, (err, user) => {
        if (err) throw err;
        if (!user) {
            return res.json({success: false, msg: 'User not found'});
        }
        res.json({ avatar: user.avatar });
    });
});

module.exports = router;



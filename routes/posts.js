const express = require('express');
const router = express.Router();
const Post = require('../models/post');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, './uploads');
    },
    filename: (req, file, callback) => {
        callback(null, file.originalname);
    }
});

const fileFilter = (req, file, callback) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        callback(null, true);
    } else {
        callback(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter,
});

router.post('/add', upload.single('attached-img'), (req, res) => {

    let newPost = new Post({
        content: req.body.content,
        creator_id: req.body.creator_id,
        creator_name: req.body.creator_name,
        post_date: req.body.post_date,
        rate: 0,
        users_liked: [],
        image: 'http://localhost:3000/uploads/' + req.file.filename
    });

    Post.addPost(newPost, (err) => {
        if (err) {
            res.json({success: false, msg: err._message});
        } else {
            res.json({success: true, msg: 'Post successfully added'});
        }
    });



});

router.get('/fetch', (req, res) => {
    Post.find((err, docs) => {
        res.json({posts: docs});
    });
});

router.get('/order', (req, res) => {
    Post.find((err, docs) => {
        res.json({posts: docs});
    }).sort({rate: 1});
});

router.post('/delete/:id', (req, res) => {
    Post.deleteOne({_id: req.params.id}, (err) => {
        if (err) {
            res.json({success: false, msg: err._message});
        } else {
            res.json({success: true, msg: 'Post successfully deleted'});
        }
    });
});

router.post('/edit/:id', (req, res) => {
    Post.updateOne({_id: req.params.id}, {content: req.body.content}, (err, obj) => {
        if (err) {
            res.json({success: false, msg: err._message});
        } else {
            res.json({success: true, msg: 'Post successfully edited'});
        }
    });
});

router.post('/rate/:id', (req, res) => {
    Post.updateOne({_id: req.params.id}, {$inc: {rate: 1}, $push: {users_liked: req.body.creator_id}}, (err, obj) => {
        if (err) {
            res.json({success: false, msg: err._message});
        } else {
            res.json({success: true, msg: 'Post rated'});
        }
    });
});

router.post('/revert/:id', (req, res) => {
    Post.updateOne({_id: req.params.id}, {
        $inc: {rate: -1},
        $pull: {users_liked: req.body.loggedUser_id}
    }, (err, obj) => {
        if (err) {
            res.json({success: false, msg: err._message});
        } else {
            res.json({success: true, msg: 'Rate reverted'});
        }
    });
});

module.exports = router;

const mongoose = require('mongoose');

const PostSchema = mongoose.Schema({
    content: {
        type: String,
        required: true
    },
    creator_id: {
      type: String,
      required: true
    },
    creator_name: {
        type: String,
        required: true
    },
    post_date: {
        type: Number,
        required: true
    },
    rate: {
        type: Number,
        required: true
    },
    users_liked: {
        type: Array,
        required: true
    },
    image: {
        type: String,
        required: true
    }
});

const Post = module.exports = mongoose.model('Post', PostSchema);

module.exports.addPost = (newPost, callback) => {
    newPost.save(callback);
};

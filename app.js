const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');

mongoose.connect(config.database);

mongoose.connection.on('connected', () => {
    console.log('\x1b[33m%s\x1b[0m', 'Connected to database.');
});

mongoose.connection.on('error', (err) => {
    console.log(`Database error: ${err}`);
});

const app = express();

const users = require('./routes/users');

const posts = require('./routes/posts');

const port = process.env.PORT || '3000';

app.use(cors());

app.use(express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static('uploads'));

// const multer = require('multer')().single();
// // app.use(multer);

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/users', users);

app.use('/posts', posts);

app.get('/', (req, res) => {
    res.send('Invalid endpoint');
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.listen(port, () => {
   console.log(`Server started on port ${port}`);
});


